What's this?
------------

An OpenTTD NewGRF for town names based on names from the Fediverse.

How do I use it?
----------------

You can compile it with nmlc to get `feditownnames.grf`, which you can then put in OpenTTD's newgrf directory.

Alternatively, you can grab the latest built NewGRF from GitLab's job artifacts [here](https://gitlab.com/lumi/feditownnames/-/jobs/artifacts/master/raw/feditownnames.grf?job=buildnml).

Can I contribute?
-----------------

Certainly! You can edit `feditownnames.nml` to add your name, more prefixes, more suffixes, more locations and whatnot.

The format is pretty simple, you can add `text("stuff", probability)` in any of the lists and it will choose to use the text "stuff" with that probability.

You can reference other `town_names` blocks inside the blocks, as well, using `town_names(identifier, probability)`, which picks the `town_names` block with that identifier with that probability.

An item with a probability of 12 will occur 4 times as often as an item with a probability of 3 and 2 times as often as an item with a probability of 6.
